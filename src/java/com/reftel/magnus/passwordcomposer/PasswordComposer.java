package com.reftel.magnus.passwordcomposer;

import java.util.Random;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.StringItem;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.MIDlet;
import com.twmacinta.util.MD5;

public class PasswordComposer extends MIDlet implements CommandListener
{
	private Form form;
	private TextField domain;
	private TextField password;
	private StringItem result;
	Command exit=new Command("Exit", Command.EXIT, 0);
	Command calculateCommand=new Command("Calculate", Command.SCREEN, 1);

	public void calculatePassword()
	{
		MD5 md5=new MD5();
		md5.Update(password.getString() + ":" + domain.getString());
		result.setText("Password: " + md5.asHex().substring(0, 8));
	}

	public PasswordComposer()
	{
		form=new Form("PasswordComposer");
		domain=new TextField("Domain", "", 80, TextField.ANY);
		password=new TextField("Master Password", "", 20, TextField.PASSWORD);
		result=new StringItem(null, "");
		form.append(domain);
		form.append(password);
		form.append(result);
		form.addCommand(exit);
		form.addCommand(calculateCommand);
		form.setCommandListener(this);
	}

	public void startApp()
	{
		Display.getDisplay(this).setCurrent(form);
	}

	public void pauseApp()
	{
	}

	public void destroyApp(boolean unconditional)
	{
	}

	public void commandAction(Command c, Displayable d)
	{
		if(c==exit) {
			notifyDestroyed();
		} else if(c==calculateCommand) {
			calculatePassword();
		}
	}
}
