# Introduction
This is the source to Password Composer Mini, an implementation of the
[Password Composer][pc] algorithm as a Java MIDlet. The implementation is
rather trivial, with the exception of the MD5 support, which is taken from the
[Fast MD5 Implementation][fm].

# Installation
The installation procedure will likely vary from phone to phone. For the phones
I've tested with (SonyEricsson k610i and P1), it was enough to send
passwordcomposer.jar to the phone via Bluetooth and pressing OK a few times on
the phone. Please see your phone manual if this does not work for you.

# Building
To build the MIDlet, you will need to place the CLDC and MIDP APIAPI jars in
the lib directory, long with the [Proguard][pg] 4.2 jar. After that, you should
be able to build the project by running "make". If you have
[MicroEmulator][me], you can place its jar in lib and use the versions of the
CLDC and MIDP jars that ships with it instead of the official stub ones and run
the MIDlet in the emulator by running "make run".

# License
Everything under src/java/com/twmacinta is copyrighted 1996 by Santeri
Paavolainen and 2002 - 2005 by Timothy W Macinta. The code is released under
the LGPL. See doc/lpgl-2.1.html for details. The rest is copyright 2008 by
Magnus Reftel and released under the 2-clause BSD license. See doc/COPYING.txt
for details.

# References
[pc]: http://www.xs4all.nl/~jlpoutre/BoT/Javascript/PasswordComposer/
[pg]: http://proguard.sourceforge.net/
[me]: http://www.microemu.org/
[fm]: http://www.twmacinta.com/myjava/fast_md5.php
